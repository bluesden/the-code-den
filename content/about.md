---
title: "About"
searchHidden: true
---

HazardLabs is the name in which I publish my programming and technical content in the internet. My 
goal is to be able to share knowledge, experience, and projects that I really enjoy, while also 
creating a community of very passionate engineers that want to build a better ecosystem.

I mostly enjoy programming in low-level languages, because of how I can interact with the hardware
and the performance benefits it gives me. I get motivated to create better software from the ones 
that I have used, and that gave me a poor experience. Performance, documentation and cohesiveness 
are core aspects when I create a program, and the decision of tooling and programming languages are 
a big aspect for me. These small details matter, and building upon them we can create better 
software.

Gaming is probably my biggest passion. I love playing on handheld consoles and the computer. I also
enjoy modding my favorite games, which are mostly sandbox games that offers a ton of fun for a 
lifetime. [The GameLabs Project](https://gitlab.com/gamelabs-project) is a group that I use to 
publish all my gaming related projects, so you can find what I played or modded.

I also enjoy self hosting services and game servers, in my homelab. I like the direct contact with
hardware and the organization aspects that a homelab needs (as I also enjoy organizing stuff too).

This blog uses [Hugo static site generator](https://gohugo.io/), with a custom theme forked from 
[yihui's hugo-xmin](https://github.com/yihui/hugo-xmin). The blog's contents can be found [in my Gitlab](https://gitlab.com/bluesden/the-code-den).

All of the blogs contents are licensed under the MIT license.

Links:
+ [itch.io](https://hazardlabs.itch.io/)
+ [Gitlab](https://gitlab.com/thearktect)
