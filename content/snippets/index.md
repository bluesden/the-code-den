---
title: "Snippets"
---

[types.h](types.h)

{{< code types.h c >}}

[types.hpp](types.hpp)

{{< code types.hpp cpp >}}

[init.lua](init.lua)

{{< code init.lua lua >}}

[dynamic_array.h](dynamic_array.h)

{{< code dynamic_array.h c >}}

