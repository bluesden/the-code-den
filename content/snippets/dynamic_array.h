#pragma once

#include <stdlib.h>

#define ARRAY_GROW_FACTOR 2
#define ARRAY_DEFAULT_SIZE 10

typedef struct {
    size_t count;
    size_t capacity;
} ArrayHeader;

#define array_header(array) ((ArrayHeader *)(array) - 1)
#define array_count(a) array_header(a)->count
#define array_capacity(a) array_header(a)->capacity
#define array_append(a, item) \
    ((a) = (array_may_realloc(a, sizeof *(a), ARRAY_DEFAULT_SIZE)), (a)[array_header(a)->count++] = (item))
#define array_last(a) (a)[array_header(a)->count - 1]
#define array_set_capacity(a, capacity) ((a) = array_may_realloc(a, sizeof *(a), capacity))
#define array_unordered_remove(a, at) ((a)[at] = array_last(a), array_header(a)->count--)
#define array_free(a) ((a) ? free(array_header(a)) : (void)0)

void *array_may_realloc(void *buffer, size_t type_size, size_t min_capacity) {
    if (buffer == NULL) {
        void *new_buffer = (void *)realloc(buffer, (min_capacity * type_size) + sizeof(ArrayHeader));
        new_buffer = (char *)new_buffer + sizeof(ArrayHeader);
        array_header(new_buffer)->count = 0;
        array_header(new_buffer)->capacity = min_capacity;
        return new_buffer;
    }

    ArrayHeader *header = array_header(buffer);

    if (header->capacity < min_capacity) {
        header->capacity = min_capacity;      
        void *new_buffer = (void *)realloc(header, (type_size * header->capacity) + sizeof(ArrayHeader));
        return (char *)new_buffer + sizeof(ArrayHeader);
    } 

    if (header->count == header->capacity) {
        header->capacity *= ARRAY_GROW_FACTOR;
        void *new_buffer = (void *)realloc(header, (type_size * header->capacity) + sizeof(ArrayHeader));
        return (char *)new_buffer + sizeof(ArrayHeader);
    }

    return buffer;
}
