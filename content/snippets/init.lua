local create_command = vim.api.nvim_create_user_command
local create_keymap = vim.api.nvim_set_keymap
local o = vim.opt
local g = vim.g

-- Colorscheme is located under ./colors/
vim.cmd("colorscheme phoenix")
vim.cmd("set clipboard+=unnamedplus")

--
-- lazy.nvim
--

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    local lazyrepo = "https://github.com/folke/lazy.nvim.git"
    local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
    if vim.v.shell_error ~= 0 then
        vim.api.nvim_echo({
            { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
            { out, "WarningMsg" },
            { "\nPress any key to exit..." },
      }, true, {})
      vim.fn.getchar()
      os.exit(1)
  end
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    spec = {
        {
            "windwp/nvim-autopairs",
            event = "InsertEnter",
            config = true
        },
        {
            "Pocco81/auto-save.nvim"
        }
    },
    checker = { enabled = false },
})

-- Configs
o.splitright = true
o.updatetime = 50
o.encoding = "utf-8"
o.mouse = "a"

o.number = true
o.relativenumber = true
o.colorcolumn = "100"

-- Don't wrap to newline.
o.wrap = false

--
-- Tabs/spaces
--

o.smartindent = true
o.expandtab = true
o.smarttab = true
o.softtabstop = 0

o.tabstop = 4
o.shiftwidth = 4

vim.api.nvim_create_autocmd({ "BufEnter" }, {
    pattern = { "*.*" },
    callback = function(env)
        o.expandtab = true
        o.softtabstop = 0
        o.tabstop = 4
        o.shiftwidth = 4
    end
})

vim.api.nvim_create_autocmd({ "BufEnter" }, {
    pattern = { "*.rb", "*.dart" },
    callback = function(env) 
        o.expandtab = true
        o.softtabstop = 0
        o.tabstop = 2
        o.shiftwidth = 2
    end
})

vim.api.nvim_create_autocmd({ "BufEnter" }, {
    pattern = { "*.go" },
    callback = function(env) 
        o.expandtab = false
        o.tabstop = 4
        o.softtabstop = -1
        o.shiftwidth = 0
    end
})

vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
    pattern = { "*.json.dist" },
    callback = function(env) 
        vim.bo.filetype = "json"
    end
})

-- Scrolloff
o.scrolloff = 8
o.sidescrolloff = 8
o.hidden = true

--
-- Spell checking
--

o.spelllang = { "en_us", "cjk" }

create_command("Spellon", function()
    o.spell = true
end, {})

create_command("Spelloff", function()
    o.spell = false
end, {})

--
-- File searching
--

o.ignorecase = true

---@param window_name string
---@return boolean
local function window_exists(window_name)
	for _, win in pairs(vim.fn.getwininfo()) do
		if win[window_name] == 1 then
            return true
		end
	end

    return false
end

-- Example usage:
-- :Search define *.h
-- :Search int*   **/*.c
create_command(
    'Search',
    function(opts)
        if opts.fargs[1] == nil then
            print("Provide a pattern to search.")
            return
        end

        if opts.fargs[2] == nil then
            print("Provide the location to search.")
            return
        end

        vim.cmd("silent grep! " .. opts.fargs[1] .. " " .. opts.fargs[2])
        if not window_exists("quickfix") then
            vim.cmd("vert copen 100")
        end
    end,
    { nargs = "*" }
)

--
-- Keymaps
--

vim.cmd "nnoremap <c-j> :m .+1<CR>=="
vim.cmd "nnoremap <c-k> :m .-2<CR>=="

create_keymap("n", "gh", "gT", {})
create_keymap("n", "gl", "gt", {})
create_keymap("t", "<C-[>", "<C-\\><C-n>", {})
