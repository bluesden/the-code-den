---
title: "Git: Setting multiple ssh keys for Git"
date: 2022-02-13T14:27:47-03:00
tags: ["Git", "ssh"]
---

Most of the times developer’s will need to use more SSH keys, maybe your company uses different services than you do for your personal projects, maybe you want to use one SSH key per project, but how do we manage that with git?

The answer is, we don’t, this configuration is done on OpenSSH which is the program that offer’s us all the implementation of SSH encryption protocol. Most systems (like macOS and Linux) comes with OpenSSH by default.

## About OpenSSH

OpenSSH is developed by the OpenBSD team. OpenBSD is a Unix-like operating system regarded for it’s security and robustness, and the same goes for their projects like OpenSSH. The guys at OpenBSD have (probably the best) man pages out there and if you want a little more depth on the project access:

- [OpenBSD website](https://www.openbsd.org/) (If you like Unix-like operating systems and more terminal-centric workflow, do give OpenBSD a try, it’s an amazing system.)
- [OpenSSH website](https://www.openssh.com/)
- [All OpenSSH manual pages](https://www.openssh.com/manual.html)

## Generating keys

Now, we can proceed and create new SSH keys. This is a trivial process but the [Github article: generating a new SSH key](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) is a great place to start if you don’t remember/know how to.

One important thing is to know what type of ssh key you are creating, the Github article talks about the two possible ones we can create: `ed25519` (the preferred one) and `rsa`. This will be important when we start configuring the multiple keys. 

## Creating the config file

By default OpenSSH will use the `~/.ssh` path to locate your config file, and will offer the same path to create your ssh keys, inside this folder create a file named `config`.

Now you can start editing it, the syntax is pretty straightforward, here is an example: 

```bash
Host github.com
        HostName github.com
        IdentityFile ~/.ssh/github
        IdentitiesOnly yes
```

In this file we are basically determining that:

- `Host` is your alias for the website (more on that later).
- `HostName` is the real website name you’ll be interacting with.
- `IdentityFile` points to the private key.
- `IdentitiesOnly` tells openSSH to only look to the identity files given in the specific config in 
this case "github"

With that entry done, once you have a remote repository in git that points to `github.com` the appropriate key will be called, you can have multiple different services too:

```bash
Host github.com
        HostName github.com
        IdentitiesOnly yes
        IdentityFile ~/.ssh/github

Host gitlab.com
        HostName gitlab.com
        IdentitiesOnly yes
        IndeitityFile ~/.ssh/gitlab
```

That’s good, we’ll now be able to have one SSH key for Github and Gitlab, you can have 2 or more repositories each pointing to a different service and you won't have any trouble, but what if you have to use 2 or more SSH keys just inside Github? This is where aliases come in.

## Host aliases

You can define your own aliases on Host names so you can have multiple SSH keys that point to only one service, imagine that you have 2 SSH keys on Github:

```bash
Host project1.github.com
        HostName github.com
        IdentitiesOnly yes
        IdentityFile ~/.ssh/github-project1

Host project2.github.com
        HostName github.com
        IdentitiesOnly yes
        IdentityFile ~/.ssh/github-project2
```

As you can see we have prepended the Host name with the project name, this will give us aliases to use inside our git repositories. We can call the Host anything, we don’t need to use the service name in it (in this case github.com), it’s just there to clarify things.

Now inside you git repository you can change you remote url to match the Host alias, so imagine that your project1 folder just points to this places:

```bash
git remote -v # List the remote repositories
origin	git@github.com:your_user_name/project1.git (fetch)
origin	git@github.com:your_user_name/project1.git (push)
```

We now have an alias just to this project, but still our remote origin points to github.com, we need to change it so it matches our alias `project1.github.com`

```bash
git remote set-url origin git@project1.github.com:your_user_name/project1.git
```

Now every time we fetch and push inside this repository it will search for the right SSH key (in this case github-project1), we can define aliases to any service as long as we give the correct HostName so OpenSSH can point to the correct place afterwards.

You can then, go to your second project and repeat the same process, just making sure to name the remote repository like the alias you intend it to use.

## Dealing with rsa keys

Remember that the type of key you choose is important? Most websites this days use `ed25519` which is the more secure and recommended one, but some services may still use `rsa`, in this case, many Linux systems (specially Fedora) and Git on Windows (Git Bash) won’t want to use this keys by default.

To use an rsa key we need to specify some parameters inside the Host config we want. By the time of the original writing, Microsoft Azure only works with rsa keys, this is from my personal config:

```bash
Host ssh.dev.azure.com
        HostName ssh.dev.azure.com
        IdentitiesOnly yes
        PubkeyAcceptedAlgorithms +ssh-rsa
        HostkeyAlgorithms +ssh-rsa
        User git
        IdentityFile ~/.ssh/azure
```

We need to pass `PubkeyAcceptedAlgorithms` and `HostkeyAlgorithms` the parameter +ssh-rsa so that OpenSSH accepts to communicate through rsa keys.

**Warning**, depending on your system version of OpenSSH and other system configurations, passing this 2 parameters inside your ssh `config` file will give you an error when some program communicate with OpenSSH, in this case simply remove or just comment the lines with a `#`.
## Git Config

After version 2.10 git now has a native method of choosing SSH keys, even though it’s not as powerful like using OpenSSH, it’s still an option. To use this method we need to have an already cloned repository, meaning that we are probably going to clone our repository with HTTP and change to SSH later on.

To add the key only for that specify repository, we need to use this command inside the repository:

```bash
git config core.sshCommand "ssh -i ~/.ssh/your-private-key"
```

This config will be available to be changed under `your-repo-name/.git/config` which is a file similar to the system-wide `.gitconfig` but is only accessed by this specific repository.
