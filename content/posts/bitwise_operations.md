---
title: "Low Level: Bitwise Operations"
date: 2023-03-07
lastmod: 2024-04-28
tags: ["Low Level"]
---

In counterpart with logical operations, such as && (and), || (or), ! (not), that you would normally
use in `if` statements, we have bitwise operations, that operate at the smallest level known to our
computer, bits.

We can use bitwise operations to make our code faster and smaller in some places. All the examples
here assume C syntax, and should work for most languages on the C-family.

# AND &
Bitwise AND compares two sets of bits and returns `1` if both compared bits are `1` otherwise it
returns `0`.

Example:

```text
0011
0101 & // Mask
------
0001   // Result
```

AND can be useful when you need to evaluate (extract) if some given bits are set. Imagine that
inside a number you have a convention that if the last bit is set you'll need to take other code
paths:

```cpp
// Check if the last bit of a 4 bit number is setted.
bool can_make_operation(int n) {
    // The extra zeroes besides 1 do nothing, they are just visual.
    int mask = 0b0001
    if (n & mask) {
        return(true);
    }

    return(false);
}
```

We can also work with the bitwise shift to see if a given bit in the middle of the word is set:

```cpp
bool check_fourth(int n) {
    // 1 before shift = 0001
    // 1 after shift  = 1000
    if (n & (1 << 4) {
        return(true);
    }

    return(false);
}
```

# OR |
Bitwise OR compares two sets of bits and returns `1` if any bit is set.

Example:

```text
0101
0010 | // Mask
------
0111   // Result
```

OR is very useful if you need to set a bit. You can create a mask and OR will only set the bit if
its not already set, while keeping all other bits unchanged.

# XOR ^
Exclusive OR compares two sets of bits and returns `1` *ONLY IF* one of the two bits is set.

Example:
```test
0101
0011 ^ // Mask
------
0110   // Result
```

XOR is very useful when we need to clear a specific bit, for example: We want to clear the 7th bit:

```text
01011001
01000000 // Mask
00011001 // Result
```

This operations is the same as taking our mask applying `NOT ~` and in sequence `AND &`

# NOT ~
Bitwise NOT inverts all the bits found inside a given number.

Example:

```text
0011 ~
------
1100 // Result of applying not
```

Code example:
```cpp
int a = 5;
int b = ~a;
```

# Bit shifting << >>
Bit shifting, as the name implies, changes the location of the 1's found inside a number. Effectively
**shifting right divides by two** and **shifting left multiplies by two**.

We can shift a number many times in a row by specifying a number higher than 1 when using either
shifting operations.

```cpp
// Left shift multiplies by 2

// 5 * 2 = 10
int b = 5 << 1;
// 5 * 2 = 10 * 2 = 20
int c = 5 << 2;

// Right shift divides by 2

// 10 / 2 = 5
int d = 10 >> 1;
```

When shifting the binary number will look like:
```cpp
// Left
0b0011 << 1
------
0b0110

// Right
0b1010 >> 1
------
0b0101
```

Shifting operations are useful to check if certain bits in the middle of a number is set, or create
a mask of bits out of another one, but, if you shift the bits out of range, you effectively lost
them:

```cpp
0b1000 << 1
------
0b0000
```

# Assignment 

All bitwise operations can be assigned, instead of creating a dedicated variable to serve as a mask
for your operations. You can prefix the `=` operator with the bitwise operation of your choice:

```cpp
int a = 5;
a &= 3; // Apply the AND operator in place, this returns the result of `5 & 3` to `a`.
```

All bitwise assignments:
```cpp
&=
|=
^=
~=
```

# Bit Flags

Bitwise operations are great to define flags. You can drastically reduce memory usage by compacting
a struct full of booleans to a simple integer. For example, instead of doing this:

```cpp
struct Player {
    bool dlcEnbled;
    bool allAchievement;
    // various other flags here
}
```

You can simply replace this with an integer that has sufficient bits that can replace all your
booleans.

- 0b000000**0**

The least significant bit highlighted here can be used to store the `dlcEnabled` flag, and
subsequent bits can be used to store other flags:

```cpp
// A more idealized version of a player struct
typdef struct {
    char name[20];
    int model_id;
    int flags; // Each bit represents a flag
} Player;
```

To manipulate each bit flag inside a number we can use a combination of enums and some simple
functions:

```cpp
// Here every enum correspond to the exact bit for each flag.
// Using enums help us because we can now identify by name every flag.
typdef enum {
    FLAG_1 = 1,
    FLAG_2 = 1 << 1,
    FLAG_3 = 1 << 2,
} Flags;

// Some example functions of how you can manipulate your players flags.
// Ideally this could be changed into a macro or be inlined by the compiler.
bool bit_flag_check(Player *player, Flags flag) {
    return(player->flags & flag);
}

void bit_flag_set(Player *player, Flags flag) {
    player->flags |= flag;
}

void bit_flag_unset(Player *player, Flags flag) {
    player->flags & (~player->flags);
}

int main(void) {
    // Initilize the flags to zero.
    Player player = { "someName", 0, 0 };
    bit_flag_set(&player, FLAG_1);

    return(0);
}
```

So that you can visualize the changes on the flags as you set and unset them, we can use this print
function:

```cpp
void print_bin(int number) {
    for (int i = 31; i >= 0; i--) {
        int bit = number & (1 << i) ? 1 : 0;
        printf("%i", bit);
    }
    printf("\n");
}
```
Here we are hard coding the for loop assuming the int has 32 bits, you could create a macro or a C++
template function to get the correct type size instead of casting or always assuming the same size.

The output for the function call `print_bin(2)` is:
```text
00000000000000000000000000000011
```

