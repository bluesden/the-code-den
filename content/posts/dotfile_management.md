---
title: "Dotfile management"
date: 2023-12-28T10:14:00-00:00
tags: ["System"]
---

Dotfiles is a popular terminology that refers to configuration files and folders that reside inside the operating system users directory (mostly in Unix-like operating systems), that start with a dot '.'

Some example of dotfiles are:

- `.bashrc` (Bash configuration file)
- `.config/` (Common configuration folder, some applications, like Neovim, use it)
- `.gitconfig` (Git configuration file)

Dotfiles are very important to automatize, customize and make a better overhaul individual experience inside your system. They can be very simple, comprising a little amount of apps, or huge configs with more than 20 apps. A quick search for `dotfiles` on GitHub shows more than 200k repositories, from personal configurations to example defaults (there's plenty of inspiration here).

If you use applications like Git, Vim/Neovim, tmux, shells (like bash, zsh, fish), terminal emulators (like Alacritty or Kitty), you probably already have some default dotfiles, and if you started experimenting with some configurations and liked some of those, its a clever idea to save them.

Imagine every time you have a problem in your computer, or maybe you experimented too much with your configs, and can't get back to what you started? It's time to manage your dotfiles.

## Managing your dotfiles

There are a bunch of ways to manage your dotfiles, you can use symlinks, make manual copies, or use specialized programs like [chezmoi](https://github.com/twpayne/chezmoi). I personally prefer to maintain a git repository with some shell scripts. It may seem antiquate, but the managing scripts and the dotfiles themselves are on the same place, and these are very simple to maintain.

First I create a git repository, where the master/main branch will remain empty, each branch is used for different users/configurations/computers. I can maintain my Windows dotfiles alongside my MacOS dotfiles separated by branches. If I use `git branch` inside my repository, it'll look like this:

- master
- linux
- windows11
- macbook

When I need a new configuration, I simply clone the repository, and create a new branch from the master branch using `git checkout -b new_branch_name`. Of course you can create a `linux_main` branch for all your Linux dotfiles and use all common configurations throughout them and only merge what you need. All the rules are up to you, its a highly flexible approach.

Inside each branch that is not my master branch, I have two files: 

- `install`: Install script just installs the existing configurations to the systems, it's used when you clone the repository the first time in your freshly installed system, but it can be used to override the configuration you already have.
- `update`: Update takes the changes from your system and updates them to your repository, making them visible to git so that you can commit the changes.

Both scripts use basic system utilities like `rm` and `cp`. Unless you are in a highly minimal system, you'll probably have all of those tools out the box, so its an almost zero dependency dotfile manager (aside from Git). Here are some snippets from my MacOS `install` and `update` scripts:

install
```bash
# nvim
mkdir -p $HOME/.config/nvim/
cp -R .config/nvim/ $HOME/.config/nvim/

# intellij
install .ideavimrc $HOME/
```

update
```bash
# neovim
cp -R $HOME/.config/nvim/ .config/nvim/

# intellij
install $HOME/.ideavimrc .ideavimrc
```

When installing, I try to create the folders beforehand because I usually pull my dotfiles repository before installing most programs. For files I simply copy/update them, for folders I use recursive copy. If some undesired files or folders are inside the recursively copied folders, you can use a top-level `.gitignore` file to ignore them. For the update script, I simply update the repository files with the system files, all the files copied mirrors the user directory hierarchy.

The advantages of using this combination is:

1. I have two copies of my dotfiles, one on the system, and one inside my repository (both remote and local). 
2. I can pull my dotfiles repository and just use the `install` script to install everything. 
3. It makes it easy for me to experiment and change my dotfiles, and only commit the changes once everything is working using the `update` script. If something goes terribly wrong, I can just use the `install` script again.

This approach may not be the best fit for huge dotfiles, because you'll need to write inside the `install` and `update` all the necessary rules. The good part is that you can change the shell scripts to your own accord, create variables and utilities to make management easier, merge both files to one and have a `install()` and `update()` function. You're only limited by your creativity. 

