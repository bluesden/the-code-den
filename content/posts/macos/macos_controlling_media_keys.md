---
title: "Controlling Media keys on MacOS"
date: 2023-10-01T10:20:00-00:00
tags: ["MacOS"]
---

In a recent quest to create an app that can potentially make me lazier, I tried to control my new MacBook Pro media keys remotely through my phone. 

If you’re a Linux user, you’ve probably heard about [KDE Connect](https://kdeconnect.kde.org/), it has a bunch o features, including pausing/playing media and increasing/decreasing your computer’s volume. All that’s fantastic but KDE Connect is really a Linux app, it works for Windows decently too (but it’s not really official) and the MacOS version is really rough on the edges.

My idea here is to create a simple app for Android/iOS that can connect to my Mac, and let me control the media keys. We don’t have any straightforward API’s to control the play/pause button (which is my main problem with the media keys, since the volume does have an API), which means that we’ll need to simulate key presses.

Before I jump in the solution, I want to make a tour through some simple discoveries I made along the way while creating the app.

### How key codes work
Basically your keyboard send signals to your operating system when you press a key (in this case a press and release event), the kernel receives it and transform it to a `virtual key code`. 

This virtual key code is platform independent, meaning that even if two different keyboards with different layouts, pressing the same key that may send different physical signals, will be transformed to the same virtual key code that your application can understand.

Apple frameworks, like Carbon/Cocoa should let us access some of these functionalities, and as expected Carbon provides us with virtual key codes but Cocoa don’t ¯\\\_(ツ)\_/¯, even though Carbon has been [discontinued](https://en.wikipedia.org/wiki/Carbon_%28API%29) in MacOS 10.15 (Catalina) it seems that this is the only way to find the current working virtual key codes.

But what is interesting is that the key codes present under:
```text
/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/HIToolbox.framework/Versions/A/Headers/Events.h
```

Which is a Carbon header, apparently doesn’t have any play/pause key, but all other media keys are present, so it’s time to intercept the virtual key codes from the system and analyze each key code in order to see the real values.

### Intercepting MacOS key presses
First I needed an app to watch for the keys I was pressing, the first solution I found was [caseyscarborough/keylogger: A no-frills keylogger for macOS.](https://github.com/caseyscarborough/keylogger), I made some simple tweaks to get only the number associated with the keys pressed and it worked fine, but once I did media key presses the application didn’t print anything.

At first sight this was weird so I downloaded [Key Codes](https://apps.apple.com/us/app/key-codes/id414568915?mt=12) on the App Store to make sure that this wasn’t a problem with the program or with the modifications I did, but it was all the same, every key press was registered but no media key presses were.

### Media events
That’s when this Github gist comes in:  [mediakeys.py](https://gist.github.com/alexkli/7226378), I actually found this when I was starting the project, but I didn’t really understand that the `NX_KEYTYPE_PLAY` was a special key handled in a special way until I fiddled around with the virtual key codes and captured the key presses.

In this gist there’s an `NSEvent` being created, and if we look at the [NSEvent documentation](https://developer.apple.com/documentation/appkit/nsevent), we can find two pretty interesting functions (going forward all of my examples will be in Swift):
- NSEvent.keyPress (which creates a key press event)
- NSEvent.otherEvent (which creates ‘other’ types of events)

If we analyze the arguments of both functions and what the gist is doing (that by way, does really pause media when used), `NSEvent.keyPress` receives a `keyCode` which is a `UInt16` and the `NSEvent.otherEvent` uses the `data1` and `data2` that receives an `Int`, so fundamentally they are dealing with different types of data and different events.

But how can we be more sure?

If we look at `NX_KEYTYPE` variables inside `IOKit.hidsystem` (to find this file, just import IOKit on your Swift application and jump to the definition of an NX_KEYTYPE key in your code) they are all `Int32`, they don’t directly translated to an `Int` (unless you are in a 32bit platform, which modern Mac’s aren’t) So as we all know `Int32` and `UInt16` are still fundamentally different.

Looking inside `IOKit.hidsystem` I found the comment which states that media key events are special:
```swift
/*
 * Special keys currently known to and understood by the system.
 * If new specialty keys are invented, extend this list as appropriate.
 * The presence of these keys in a particular implementation is not
 * guaranteed.
 */

public var NX_NOSPECIALKEY: Int32 { get }
public var NX_KEYTYPE_SOUND_UP: Int32 { get }
public var NX_KEYTYPE_SOUND_DOWN: Int32 { get }
/* And so on */
```

Event though it states that “a particular implementation is not guaranteed” we know that external devices (like keyboards and Bluetooth devices like the AirPods) can control the media keys, so we’ll just ignore that part.

And for the last nail in the coffin, we have [nevyn/SPMediaKeyTap: \[Cocoa\] SPMediaKeyTap is a global event tap for the play/pause, prev and next keys on the keyboard.](https://github.com/nevyn/SPMediaKeyTap/tree/master) that is an app that can intercept media key events, it’s in Objective-C and really helped me understand how these special events can be constructed so that my own application can emit them, it uses the `otherEvent` and looks for the fields `data1` when dealing with those media keys, so even though `NSEvent.keyPress` really appeals to the eyes, its not what we are looking for.

### How to create media key events
To create a media key event, we need to glue together some information's inside a variable (in this case an `int`) that will efficiently pack information like:

- key code
- key flags
- the key is being pressed or not
- the key is repeating

The binary protocol is very simple, the last 2 bytes of the number represent the key code, the first 2 its flags and other data. For example:

```swift
0xFFFF0000 // The last 4 F's can represent a key code.
0x0000FFFF // The first 4 F's can represent the flags.
```

The examples I’ll give here closely resembles the [SPMediaKeyTap program example](https://github.com/nevyn/SPMediaKeyTap/blob/master/Example/SPMediaKeyTapExampleAppDelegate.m).

Knowing that we can easily intercept any NSEvent and pickup some information:

```swift
// Imagine that we are receiving an event here

// We mask data1 to get the last 2 bytes of information, the binary
// operator & will check to see if the 2 last bytes are set, and we'll
// shift right by 16 to get only the 2 last bytes that represent our
// key code.
let keyCode = (event.data1 & 0xFFFF0000) >> 16

// We'll do the same for the flags but now only getting the first 
// 2 bytes. We don't need to shift anything as the last 2 bytes will be
// discarded as the are only zeros.
let flags = (event.data1 & 0x0000FFFF)

// Again, from the flags just the last byte will tell us if the
// key is being pressed, 0xA means pressed, 0xB means key has been
// released.
let keyPressed = (keyFlags & 0xFF00) >> 8 == 0xA

// Last but not least, the key repeat, I don't have much information, 
// but guessing, 0x1 represents if the key is being currently pressed.
let keyRepeat = keyFlags & 0x1;
```

Well that’s quite simple, this gives a good idea of how these key presses work, but I still don’t have the full picture, I haven’t found a single official documentation, all I’ve gathered where snippets, programs and some discussions on how to do it.

I still don’t really know how every argument on `NSEvent.otherEvent` works (maybe I should look at GNU Step source code to figure it out, but that’s for the future) but so far, I was able to achieve what I wanted, media key presses.

### Final code
```swift
import IOKit

let KEY_UP: Int32 = 0xB00
let KEY_DOWN: Int32 = 0xA00

func main() {
	pressNxKey(NX_KEYTYPE_PLAY)
}

func pressNxKey(key: Int32) {
    func press(_ keyState: Int32) {
		let data1 = Int((key << 16) | keyState)
        let flags = NSEvent.ModifierFlags(rawValue: 0)

        let ev = NSEvent.otherEvent(
            with: NSEvent.EventType.systemDefined,
            location: NSPoint(x: 0, y: 0),
            modifierFlags: flags,
            timestamp: 0,
            windowNumber: 0,
            context: nil,
            subtype: 8,
            data1: data1,
            data2: -1
        )
     
        let cev = ev?.cgEvent
        guard let cev else { return }
        cev.post(tap: CGEventTapLocation.cghidEventTap)
    }

	// Imitate the keyboard with press and release
    press(KEY_DOWN)
    press(KEY_UP)
}
```

You can create a simple SwiftUI app to just make a click to call the `pressNxKey` function, when you tap the button, the application will need accessibility access so that it can send this custom NSEvent.

Observation: The keys need to be pressed down and up always, some media events probably rely on some other data (like constant pressing) to work correctly, and this can be problematic. I tried only pressing down the mute key using `NX_KEYTYPE_MUTE`, it always make all my media keys unresponsive for some time, using `KEY_DOWN` and then `KEY_UP` works as expected.

### Last considerations
Basically the keylogger application could intercept the media keys too, but it was set up in a way to just intercept key presses, here we can analyze the difference between these two snippets, first the keylogger:

```c
CGEventMask eventMask = CGEventMaskBit(kCGEventKeyDown) | CGEventMaskBit(kCGEventFlagsChanged);

CFMachPortRef eventTap = CGEventTapCreate(
        kCGSessionEventTap, 
		kCGHeadInsertEventTap, 
		0, eventMask, CGEventCallback, NULL
);
```

And if we look at the Objective-C code from SPMediaKeyTap:
```objc
_eventPort = CGEventTapCreate(kCGSessionEventTap,
						      kCGHeadInsertEventTap,
							  kCGEventTapOptionDefault,
							  CGEventMaskBit(NX_SYSDEFINED),
							  tapEventCallback,
							  self);
```

The difference lies on `CGEventMaskBit`, keylogger only want’s key down events and events in which the flags have changed, while SPMediaKeyTap only cares about `NX_SYSDEFINED` (key presses and media key events are legitimately different).

Although now it seems very straightforward and easy, I wouldn’t understand  anything without both of these examples, I couldn’t really find anything about `CGEventTapCreate` until these, and the mask on `NX_SYSDEFINED` well, without real documentation, I wound’t have though it was used this way. Maybe the docs are just old and I didn’t find them.

Another way to look for those events is to use `NSEvent.addGlobalMonitorForEventsMatchingMask`, which will work with a mask to display only what you want and a handler that is the event callback, the only drawback is that it only works by observing outside events, the ones sent to your app will not be shown to you.
