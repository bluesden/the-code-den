---
title: "Linux in a TV box"
date: 2024-02-14
tags: ["Linux"]
---

This is my success story transforming a simple TV box into an armbian server. I've found this Android TV box, brought it home and (after some years) started to fiddle with it.

![TV box image](mxq1.jpeg)

It was originally an Android 7.1 with a menu that looked exactly like every other MXQ TV box I've seen. I don't really know where this "Audisat" company, which distributed this box, is from. I found two videos in YouTube made by them, teaching how to upgrade the firmware and that's it, no more info. The hardware is actually a capable one for my micro server needs:
+ CPU: Amlogic S905X
+ GPU: Mali-450 MP
+ RAM: 1GB
+ eMMC: 8GB

And as you may have noticed by the images, this TV box has more ports than most modern laptops. Here's the full list of ports:
+ 5V input
+ Ethernet
+ AV
+ SPDIF
+ HDMI
+ SD/MMC
+ x4 USB-A 2.0

After searching a bit, I found some [armbian community builds](https://github.com/ophub/amlogic-s9xxx-armbian) for the s9x series of processors. I download the ISO `Armbian_24.2.0_amlogic_s905x-t95_noble_6.1.76_server_2024.02.01`, notice the specific build has the name `s905x-t95`, I tried other images but they were boot looping. I used an SD card as most tutorials recommend it, but you could probably try to use a thumb drive too. The card memory doesn't make any difference, if it can hold the image that's enough, I used an 8GB card.

For people who doesn't know, these TV boxes are not meant to run Linux, but some clever people hacked their way around it, and automatized most of the process to install it for us, so they mostly work out-of-the-box. The steps I took were fairly simple:

1. Download the community `amlogic_s905x_t95_noble` [armbian image](https://github.com/ophub/amlogic-s9xxx-armbian) and [balenaEtcher](https://etcher.balena.io).
2. Flash the image on the SD card, remove the SD card and re-insert it into the computer.
3. Copy the `u-boot-s905x-s912.bin` inside the `BOOT` partition and rename the copy to `u-boot.ext` (you still need the `u-boot-s905x-s912.bin` file for later, don't just rename it).

Doing that I inserted the SD card and was ready to boot from it, whoever, you need your TV box to start the boot process from any external storage, we have basically two ways to do so:

1. If your TV box have a reset button, you'll need to press it when your box powers on, and it should boot into your SD card. 

2. If your TV box **doesn't** have a reset button, first check if you have a file called `aml_autoscript.zip` inside your SD card `BOOT` partition, if it doesn't exist, simply create the file and move it inside your SD card. Now we can normally boot on Android and find an app that is named like `Update & Backup` (if you don't have it, chance is that you cannot install a custom system, or your reset button is hidden inside the TV box case), there you can search for the file `aml_autoscript.zip` (some apps do this automatically, make sure your SD card is correctly plugged on your TV box), after selecting it you can just hit update, the system will reboot and from now onward, it'll try to boot from external drives.

If you wish to, you can use armbian just out of your SD card or thumb drive, without touching the system that is inside your TV box. I've opted to purge Android and install armbian directly into my TV box internal storage as my SOC has eMMC memory, which makes it very easy to install using `armbian-install`. Some systems have only NAND memory, for those cases its good to search more and look at your options. If you want to be more cautious, you can make a backup copy of the Android inside your TV box if you pretend to restore it at some point.

After booting and waiting the system to load everything, I successfully installed the system using the command `armbian-install`. The only manual process was to choose which dtb I wanted to use for the installation, I've chosen the one that has `p212` in it. Once everything was installed, I rebooted and logged as root, ready to start tweaking the system. SSH was already configured, so I just accessed it with my daily driver machine. The good thing is that the TV box works headless, meaning that you don't need any HDMI cables connect to it, you can just power it on and SSH inside it.

Some people opt to install systems like [LibreELEC](https://libreelec.tv) to watch movies over the network (which was what the machine was meant to do but with Android), some install distributions focused on emulating older consoles, but I went the server route and started configuring and providing services on my local network.

## NFS
Its pretty simple to configure NFS, under `/etc/exports` we can configure who can access which folders:

```text
/media/share 192.168.0.100(rw,sync,no_subtree_check,insecure)
```

Here I'm using a local IPv4 address to allow only this IP to access the folder `/media/share`. Between parentheses we pass the flags, you can check all flags under the manual page `man exports`. There's a particular flag 
I'm using: `insecure`, without this I'm unable to connect from MacOS, as it seems, to work without insecure, NFS needs to allow connections on non-reserved ports, which I haven't configured.

I'm also using TCP wrappers to control access to the TV box:

`/etc/hosts.allow`: Allow the local IP to access any process on the machine (like sshd).
```text
ALL: 192.168.0.100
```

`/etc/hosts.deny`: Deny ALL access to the machine, unless they are listed on `hosts.allow`
```text
ALL: ALL
```

You can now connect to NFS and access the folder you shared. On graphical interfaces you'll probably need to access via a URI like this `nfs://192.168.0.100/media/share`.

## Pacman Repo
For some time now I wanted to try and create a repository to provide packages locally. I've seen many people hosting mirrors of different distributions in their homelabs, but what sparked my initial interest was the Steam Deck. Valve create their own pacman repository to provide packages to their new SteamOS 3, which is an Arch Linux based system, to update drivers and provide a consistent gaming experience. Of course, I don't have something as big as Valve, but I wanted to understand how to do it. 

Fortunately enough, armbian Ubuntu has everything we need to host a pacman repository, you need to install:

```bash
# libarchive-tools is needed because of bsdtar

apt install libarchive-tools pacman-package-manager makepkg
```

Following the [Arch wiki](https://wiki.archlinux.org/title/Creating_packages) on how to create packages, I created two simple files to provide a shell script as a package. This is a great test package to check if the repository is working. Inside a folder I created:

`PKGBUILD` provides instructions on how to compress our package so that it can be latter installed by pacman. Because this is a local script that I'm using, most default PKGBUILD functions were ignored since we don't need to use them.
```bash
pkgname=success_script
pkgver=0.1
pkgrel=1
pkgdesc="Successfully building a package for arch linux"
arch=("any")
license=("MIT")
options=()
source=($pkgname)
sha256sums=("5dbda20950555ea9f4e8df6c2151caf8b52c50063989ddf849fe4421ffc89eca")

package() {
	install -Dm 755 $pkgname -t "$pkgdir/usr/bin"
}
```

The only real work here is to get the sha256sum, most distros already have this tool available, just run `sha256sum filename`, where `filename` is the name of our script, the output hash is what we provide on sha256sums.

`success_script` is our executable. Just a simple script to run, so that we can make sure that the package was installed successfully
```bash
#!/bin/env bash

echo "Success script!"
```

We can now run `makepkg`, which will archive our program. The resulting file name should look like this `success_script-0.1-1-any.pkg.tar.gz`.

Now once again following the [Arch wiki](https://wiki.archlinux.org/title/Pacman/Tips_and_tricks#Custom_local_repository) on how to set our local repository, we can use the `repo-add` command. Basically we are going to create a repository database that is going to provide the packages, and once we have a new or updated package we can use `repo-add` to update our database. I created a folder `/repo` to store my database:

```bash
mkdir /repo

repo-add repo.db.tar.gz /packages/success_script/success_script-0.1-1-any.pkg.tar.gz
```

Before providing any packages, every `pkg.tar.gz` that we have must live inside our `/repo` folder, even if you are not required to use `repo-add` from within the same directory of your repository database, it's not going to be possible to provide our package unless the package archive and repository database lives in the same directory. Now we can hop into a Linux that is using pacman and change the `/etc/pacman.conf` file to add our repository, in this case we are already using pacman, and we'll add the repository from our own machine:

```bash
# Between [] we put the name of our database, in our case the database was named repo.db.tar.gz, 
# this means that our repo is called 'repo'.
[repo]
SigLevel = Optional TrustAll
Server = file:///repo/

# If you are acessing your custom repo from other computer inside 
# your local network, you can use this instead, change the IP and path accordingly
# Server = http://192.168.0.100/repo
```

Now we can see magic happening:

```bash
pacman -Syu
pacman -S success_script
success_script

output: Success script!
```

With this we can create our own repositories and expose them to [AUR](https://aur.archlinux.org) too.

## Final thoughts
Even though this is a very simple machine, it's interesting to see that its capable enough to run things that we use every day. Here's an image of it running:

![Working TV box image](mxq2.jpeg)
