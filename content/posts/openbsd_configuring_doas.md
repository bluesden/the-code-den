---
title: "OpenBSD: Configuring OpenBSD doas"
date: 2022-01-31T10:15:49-03:00
tags: ["OpenBSD"]
---

`doas` is the default OpenBSD command to run other commands with privileges, it works like Linux `sudo` and was introduced in OpenBSD 5.8.

When installing OpenBSD you must have set a root account password and your user accout. Now you'll need to log into the root account using the username `root` and the password you previously chose.

OpenBSD provides a default configuration for doas under their `/etc/examples` folder, we just need to copy it to:

```shell
cp /etc/examples/doas.conf /etc/doas.conf
```

With the file in place we can edit it and permit our normal user to use it, you can use `vi` or `mg` as they come with the system. 

At the last lines of the file you should see a line like `permit keepenv :wheel` below it you can configure your user access to the doas group:

```shell
permit keepenv nopass you_user_name
```

The command `permit` will give access to your user to the wheel group, making it possible to use doas.
The other two are:
- keepenv: keep your users environmental variables when using doas
- nopass: don't ask for a password when using doas

Both of these arguments can be kept out of the line, choose what you prefer.

The final file will look like:

```shell
# Allow wheel by default
permit keepenv :wheel
permit keepenv nopass you_user_name
```

Now you can log back to your user and test the doas command, you can do so by:
```
doas cat /etc/doas.conf
```

If you're prompted to input your password or the text inside the file is printed doas has been configured successfully!
