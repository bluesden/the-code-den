---
title: "Android: How to pass data to a previous Fragment"
date: 2023-01-21T10:15:00-00:00
tags: ["Android"]
---
# Disclaimer
**This tutorial assumes the use of Navigation component with Fragments**

# Introduction
Passing data between destinations is a common thing in Android development, we can pass data using `Bundles` (for some old styled code) and we can use Navigation component safe args to achieve a safer way of passing data between navigation's.

But when it comes to passing data to the previous Fragment that navigated to the one we are currently in, we don't really have an established way to do so, the closer utility we have is to create an Activity, start the Activity with an Intent and wait for the Activity to return with `setResult()` and `finish()`.

But here I'll show you a very simple method of combining a very modern library, Navigation component, to have virtually the same usage of the Activity `setResult()`.

# Setup
Imagine we have **FragmentA** and **FragmentB**, simply we navigate from A -> B and expect that **B** returns something so that **A** can choose what to do next.

In **FragmentA** we need to retrieve a **LiveData<T\>** that will be present on the savedStateHandle, once we navigate to **B** we'll be able to put a value inside savedStateHandle that **A** can retrieve.

```kotlin
class FragmentA() : Fragment(R.layout.a_fragment) {
    override onCreate() { /* omitted */ }

    // We need to use a function that is in a lifeCycle that can access most Fragment properties
    override onResume() {
        super.onResume()
        // Here we'll retrieve the liveData from savedStateHandle and do what we need.
        // We need to specify the type we are going to get from getLiveData<T>
        // The constant informed in the getLiveData method indicates where we'll find the
        // value we need.
        val arg = findNavController()
            .currentBackStackEntry?
            .savedStateHandle?
            .getLiveData<Boolean>(CAN_PROCEED_TO_FRAGMENT_C)

        arg?.let {
            // Here we can use .value or .observer(viewLifecycleOwner) but don't forget that
            // viewLifeCycleOwner needs to available to your current function based on the
            // Fragment's lifecycle.
            if (arg.value) {
                // goto FragmentC
            } else {
                // Fragment B returned something that is preventing us from going to FragmentC
                // do something else instead.
            }
        }
        
    }

    companion object {
        const val CAN_PROCEED_TO_FRAGMENT_C = "canProceedToFragmentC"
    }
}
```

Now inside **FragmentB** we can set the savedStateHandle and navigate back to **A**, when this happens the savedStateHandle will have a valid, non null value and `arg?.let` will execute the body.

```kotlin
class FragmentB : Fragment(R.layout.b_fragment) {
    override onCreate() { /* omitted */ }

    // Here we use the const key from FragmentA to correctly put the Boolean value 
    // with the matching key so that FragmentA can find this on the savedStateHandle, then
    // we can pop the current fragment to immediately return.
    fun someFunction(everythingOk: Boolean) {
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            FragmentA.CAN_PROCEED_TO_FRAGMENT_C,
            everythingOk
        )
        findNavController().popBackStack()
    }
}
```
# Possible errors
This method is great to pass data to a previous Fragment, but if your user can go back between FragmentA and FragmentB your logic can be broken by the fact that the savedStateHandle will not change, if thats the case we can simply remove the value that was set by **FragmentB** inside **FragmentA**

```kotlin
class FragmentA() : Fragment(R.layout.a_fragment) {
    override onResume() {
        super.onResume()
        // We can replace the old way of using getLiveData to use simply remove that will
        // return the value from the savedStateHandle in the provided key and remove the value
        // from savedStateHandle
        val arg = findNavController()
            .currentBackStackEntry?
            .savedStateHandle?
            .remove<Boolean>(CAN_PROCEED_TO_FRAGMENT_C)

        arg?.let {
            ... 
        }
    }

    companion object {
        const val CAN_PROCEED_TO_FRAGMENT_C = "canProceedToFragmentC"
    }
}
```

# Abstraction
If you want to, you can create two extensions to help you:
```kotlin
fun<T> Fragment.getNavigationResult(key: String) =
    findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<T>(key)

fun<T> Fragment.setNavigationResult(key: String, value: T) = 
    findNavController().previousBackStackEntry?.savedStateHandle?.set(key, value)
```
