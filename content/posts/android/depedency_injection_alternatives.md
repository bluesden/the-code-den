---
title: "Android: Dependency Injection Alternatives"
date: 2023-07-23T10:15:00-00:00
tags: ["Android"]
---

Dependency injection on Android (and most other places) are deemed needed and installed as libraries, some of them work at compile time (like Dagger) and others are runtime (like Koin), both put heavy constraints to our applications, for compile time ones the compile times absurdly increases, and for runtime we can only catch bugs when actually invoking the dependency injection library.

We can fully remove dependency injection libraries by using simple language constructs, making it easier and giving us more control over what we are doing, and don't fret, you'll still be able to write tests and fullfil every duty you did with said libraries.

**WARNING**: This article doesn't create replacement for singleton pattern provided by these libraries, but we have substitutes for this already, in Kotlin they are easily achieved with `Object` and for most Android applications you can just initialize whatever you need on your application class.

# Official problem

The Google team alongside with the official Android docs heavily advertise the use of Clean Architecture and some opinionated techniques and patterns for creating Android apps, and they recommend the use of dependency injection. They state that by using it you'll have some positive points:

- Reusability of code
- Ease of refactoring
- Ease of testing

All of this is based on creating a "good app architecture", but this couldn't be more vague, all the three points listed above are easily achievable with pure Kotlin constructs.

# Using default parameters

It may sound too simple, but in the end it is simplicity that wins, imagine that you are setting up a class that will use tons of other layers (you might use dependency inversion or not) and some built-in Android classes.

```kotlin
class MyClass(
    val logicLayer: LogicLayerInterface = LogicLayerImpl(),
    val apiLayer: ApiLayerInterface = ApiLayerImpl(),
    val dispatcher = Dispatchers.Default
)
```

Simple as this may seem, its all done, the class carries the logic and dependencies it needs, you don't need to go looking for another dependency injection file, deeply nested inside your dreadful app architecture nor you need to setup a complicated dependency tree, but the best thing is, you can swap everything your class depends upon if you want to test it.

```kotlin
// If you use dependency inversion, which is basically depending on the interface, 
// which the example above has shown on the default parameters, 
// we can simply implement the interfaces and create a mock.

class LogicLayerMockImpl : LogicLayerInterface {
    // Mock the methods here, or use a library that auto mocks to you.
}

fun `test MyClass with mock dependencies`() {
    val mockClass = MyClass(
        logicLayer = LogicLayerMockImpl(),
        apiLayer = ApiLayerImplMock(),
        // you don't need to provide the dispatchers if thats what you already need.
    )

    // Make any tests with your class
}
```

# Without default parameters

We are indeed blessed by default parameters in Kotlin, but imagine now that the language you are working on doesn't have said concept, like C, you can easily use functions to tackle the problem. I'll still use Kotlin for these examples:

```kotlin
// Now we can't supply default parameters
class MyClass(
    val logicLayer: LogicLayerInterface,
    val apiLayer: ApiLayerInterface,
    val dispatcher
)
```

For Kotlin we have two alternatives, we can use a companion object:

```kotlin
class MyClass(
    /* omitted */
) {
    companion object {
        fun create(): MyClass {
            return MyClass(
                logicLayer = LogicLayerImpl(),
                apiLayer = ApiLayerImpl(),
                dispatcher = Dispatchers.Default
            )
        }
    }
}

// You can simply invoke it like this:
MyClass.create()
```

Or just a simple function:

```kotlin
fun createMyClass(): MyClass {
    return MyClass(
        logicLayer = LogicLayerImpl(),
        apiLayer = ApiLayerImpl(),
        dispatcher = Dispatchers.Default
    )
}

class MyClass(
    /* omitted */
)

// Again simply call like this
createMyClass()
```

So now on your tests you can just create a function that will pass all the mock interface implementations

```kotlin
fun createMockMyClass(): MyClass {
    return MyClass(
        logicLayer = LogicLayerMockImpl(),
        apiLayer = ApiLayerImplMock(),
        dispatcher = Dispatchers.IO
    )
}
```

In here you could even supply via function parameters the dispatcher or any other dependency and tailor things specifically for your needs.

# Advantages

Some advantages by using the method above instead of dependency injection libraries:

1. Simplicity

2. No extra external dependencies

3. Compile time checks (if using dependency inversion, the compiler will ensure that the class provided implements the interface)

4. Greater control of the class dependencies (this means that you can mix and match different classes and implementations based on your apps state in a very **simple** manner)

5. No libraries setup, no major version breaking changes and no hours of scheming through documentation and stack overflow answers.

6. Only depend on basic language constructs (You'll probably not be seeing changes on Kotlin's default parameters).

# Closing notes

With just this feature we can tackle the three points provided by the Android docs on why we should be using dependency injection:

- Reusability of code: Depending on the interface already provides us that, we can mix and match interface implementations.
- Ease of refactoring: Again, using default parameters and interfaces is easy, we can use other functions to create a class using different parameters, simple and direct.
- Ease of testing: Yet again, we depend on interfaces, we can simply pass whatever we need to our class in order to test it.

# Advice on large projects

For large projects we tend to think "what's the best architecture? How can I setup the project in a reliable way?". After asking those question people find all types of answers on the internet, one of those is to use an utterly complex architecture.

Start simple, use the provided language constructs, which in Kotlin are pretty simple, and when you face a problem that needs a more sophisticated approach, you'll be sure that you made everything the simplest way possible, and introducing new techniques will be a breeze.

In the end use whats going to make your work better and take you less time and provide better tools. More libraries aren't the answer most of the times, its difficult to let go the idea that we need more libraries to be more productive than to actually implement features ourselves and save time on the future. So I'm not totally against using libraries for some work, by all means use whats better for the project, but in the beginning of the project, no one knows whats better for it.
