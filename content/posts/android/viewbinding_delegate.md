---
title: "Android: Creating a ViewBinding with delegate"
date: 2023-02-01T21:00:00-03:00
tags: ["Android"]
---

The [ViewBinding](https://developer.android.com/topic/libraries/view-binding) is the recommended way of accessing XML elements on Android layout creation. It's use is very simple, but the Android official docs recommend that we use it in a way that destroys the resources by setting our binding to null.

```kotlin
// This is the recommended way of using binding.
private var _binding: ResultProfileBinding? = null
private val binding get() = _binding!! // Looks a little ugly

override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
): View? {
    _binding = ResultProfileBinding.inflate(inflater, container, false)
    val view = binding.root
    return view
}

// A lot of overrides
override fun onDestroyView() {
    super.onDestroyView()
    // "freeing" resources
    _binding = null
}
```

As you can see, setting up binding on your fragments and activities after going through the correct steps on the Android docs on how to activate ViewBinding on your project, are very straightforward.

But we can reduce the boilerplate, totally eliminate the ugly `!!` and not override `onDestroyView` every time.

To do so, will be using the `delegate` pattern, which is supported out-of-the-box by Kotlin.

```kotlin
class FragmentViewBindingDelegate<reified T : ViewBinding>(
    private val bindingClass: T
) : ReadOnlyProperty<Fragment, T>, DefaultLifecycleObserver {
    
    private var binding: T? = null

    // This function comes from the ReadOnlyProperty, this is the same as Kotlin's get()
    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        // We need to check if binding is null because every time we 
        // call binding on our view we are going to effectively 
        // be re-creating the binding every time and the lifecycle observer.
        if (binding == null) {
            binding = bindingClass.getMethod("bind", View::class.java).invoke(null, thisRef.requireView()) as T
            thisRef.viewLifecycleOwner.lifecycle.addObserver(this)
        }

        // I kinda lied about the !!, we still use it but only here.
        return binding!!
    }

    // Function is overwritten using the DefaultLifecycleObserver interface
    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        owner.lifecycle.removeObserver(this)
        binding = null
    }
}
```
This is it for the Fragment, but if we want to use it on an Activity, we need to call the "inflate" method on `bindingClass.getMethod`. The Delegate class will be virtually the same with a minor change:

```kotlin
// Different class name but same parameters
class ActivityViewBindingDelegate<reified T : ViewBinding>(
    private val bindingClass: T
) : ReadOnlyProperty<Fragment, T>, DefaultLifecycleObserver {
    
    private var binding: T? = null

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        if (binding == null) {
            // Here we change the way we get the binding
            binding = bindingClass
                .getMethod("inflate", View::class.java)
                .invoke(null, thisRef.requireView()) as T
            thisRef.viewLifecycleOwner.lifecycle.addObserver(this)
        }

        return binding!!
    }

    override fun onDestroy(owner: LifecycleOwner) {
        // same
    }
}
```

And now we need a way to call both of these inline, to do so we can create two handy extensions:

```kotlin
inline fun <reified T: ViewBinding> Fragment.viewBinding() = 
        FragmentViewBindingDelegate(T::class.java)

inline fun <reified T: ViewBinding> ComponentActivity.viewBinding() = 
        ActivityViewBindingDelegate(T::class.java)
```
But why are these extensions? If you notice we are not going to use the Fragment or ComponentActivity at all when calling our delegate. The only good reason to use an extension here is to limit scope, if the linting gets unbearable for you, right on top of the function you can use a:

```kotlin
@Suppress("unused")

// Or if you want to do this to the entire file, just put this on your files first line
@file:Suppress("unused")
```
## Usage
Now inside you Fragment or Activity you can simply do:
```kotlin
class MyFragment : Fragment(R.layout.some_layout) {
    private val binding: MyFragmentBinding by viewBinding()
}
```

## Final code
```kotlin
inline fun <reified T: ViewBinding> Fragment.viewBinding() = 
        FragmentViewBindingDelegate(T::class.java)

class FragmentViewBindingDelegate<reified T : ViewBinding>(
    private val bindingClass: T
) : ReadOnlyProperty<Fragment, T>, DefaultLifecycleObserver {
    
    private var binding: T? = null

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        if (binding == null) {
            binding = bindingClass
                .getMethod("bind", View::class.java)
                .invoke(null, thisRef.requireView()) as T
            thisRef.viewLifecycleOwner.lifecycle.addObserver(this)
        }

        return binding!!
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        owner.lifecycle.removeObserver(this)
        binding = null
    }
}

inline fun <reified T: ViewBinding> ComponentActivity.viewBinding() = 
        ActivityViewBindingDelegate(T::class.java)

class ActivityViewBindingDelegate<reified T : ViewBinding>(
    private val bindingClass: T
) : ReadOnlyProperty<Fragment, T>, DefaultLifecycleObserver {
    
    private var binding: T? = null

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        if (binding == null) {
            binding = bindingClass
                .getMethod("inflate", LayoutInflater::class.java)
                .invoke(null, thisRef.requireView()) as T
            thisRef.viewLifecycleOwner.lifecycle.addObserver(this)
        }

        return binding!!
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        owner.lifecycle.removeObserver(this)
        binding = null
    }
}
```

This code was implemented by [Jaison Klemer](https://github.com/jaisonklemer) and [Alexandre Parra](https://github.com/duffhd).

