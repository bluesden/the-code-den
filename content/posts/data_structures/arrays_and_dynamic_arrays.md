---
title: "Data Structures 01: Arrays and Dynamic Arrays"
date: 2023-10-01T10:10:00-00:00
tags: ["data structures"]
---

In these series of blog posts, I want to give a brief overview over some data structures and some implementation ideas and caveats of each of them, giving a more "engineering" view over them and why we do what we do.


### Arrays
Arrays are one of the fundamental data structures that are present on every language as a basic type. Many other data structures expand upon arrays and in this article I'll cover how arrays work and expand upon dynamic arrays (which is the logical successor of an array).

Arrays are contiguous blocks of memory that can hold multiple items of a type T. Arrays have a fixed size, meaning that when you create an array, you need to upfront tell your programming language what size it'll have (some languages only use dynamic arrays so thats not necessary). For the purposes of showing how an array works, I'll use the C programming language which gives us a lot of control over what we do and expose us the most raw constructs so that we can expand upon them.

In C we can create an array by doing:
```c
int main() {
    int my_array[] = {1, 2, 3, 4, 5};
    return 0;
}
```

Here I created an array named `my_array`, that has type `int` and has 5 items inside it, C already understands that we want an array with size 5 because we only passed 5 int's to it, so it naturally calculates the size for us. Now we have two basic operations available to us:
- Change a value inside the array
- Retrieve a value from an specific position of the array

Both these operations use the same concept of `indexing` the array, basically we'll provide our programming language what item inside our array we want to access:

```c
// Change a value inside the array
// Arrays always start at index 0, therefore my_array
// can be index with the numbers 0 through 4 (5 items total).
int first_item = my_array[0];

// Change the first value to 0;
my_array[0] = 0;
```

This seems pretty simple, we have a contiguous storage of 5 ints, we tell the language what index we want and it just fetches the value for us, but how does that works under the hood?

It's actually very simple, first we need to understand the size of the array itself, and there is a pretty simple calculation:

`size of the array * size of the type`

The total size in bytes of the array is calculated by providing the array size times the type size, in C its pretty straightforward to discover the size type:

```c
sizeof(int);
```

The `sizeof` operator tells us how many bytes an `int` has on the current architecture that we are currently running this code. For this articles, lets assume and int has 32 bits (4 bytes).

The array size can be more tricky, but we have a nifty way in C that can tell us the array size:

```c
sizeof(my_array)/sizeof(my_array[0]);

```

Important notice: this method of getting the array is not the best, because C doesn't store any information at all about the array we just created, we need to use this trick or manually track the size of our array trough a variable, or the most archaic way, manually counting the items inside our array. If you want to know more about it, theres great answers here: https://stackoverflow.com/questions/37538/how-do-i-determine-the-size-of-my-array-in-c 

With these 2 numbers in hand we can calculate the total amount of bytes this array will consume:

5 * 4 bytes = 20 bytes

Mere 20 bytes are used, this can be a useful information, but the 5 items and 4 bytes from the int are still the most important numbers. Knowing the size of the data we can advance the array to get the next int inside it, because arrays are contiguous blocks of memory, every int ends right next to the start of the next int (only if theres no padding or data alignment, but thats for another time), so if we want to get the first int, on index 0, we read through byte 0-3, if we want the index 1, we read from byte 4-7 (always reading 4 bytes at once):

```text
 0  3 4  7 8  12
|----|----|----|
|0001|0002|0003|
|----|----|----|
0    1    2

A block of memory holding 3 values, index 0 has value 1, index 1 has value
2, index 2 has value 3. At the top we see the start and ends of the bytes.
```

So when we index a variable like this:
```c
my_array[2];
```
The compiler is basically understanding:
size of int * index
4 * 2 = 8

After knowing that the int at index 2 is on byte 8, we can read from 8 to 12 to get the int value.

The array size is useful to guarantee that we don't read memory that is out of bounds, meaning, memory that doesn't belong to the array. C doesn't have any warning for array indexing problems unless you actually run the code, because of that, it's always useful to know the array size and its type size.

This reveals to us, why arrays can only hold one type, the type size is fundamental to indexing an array, but of course we could interpret the bytes differently or do a lot of magic to store various types inside a single array, but from a CPU perspective, its just better to use arrays the way they are meant to be.

This imperatively lead many languages to adopt different solutions, some of them only use dynamic arrays, and others provide dynamic arrays which wrap around normal un-managed arrays, like C++ `std::vector`.

Even though we may have a security problem with them, arrays can be the most cost efficient in terms of memory usage (and there are lots of safe ways to use them), but when doing a lot of operations on said array (like removing items or appending), the lack of information on its size, or the ergonomic usage of the data structure, can lead to several bugs.

This inevitably leads us to dynamic arrays which I'll show a simple implementation and the rationale behind how it works.

### Dynamic arrays
Put simply, dynamic arrays are arrays that can grow or shrink in size when we add or remove items from it, these operations are called on demand, the implementation of the dynamic array is the one that is going to determine if the array itself needs more space or not.

The implementation is pretty trivial, for an example, I'll create a C structure to represent a dynamic array, the information inside our struct should be useful to make the operations inside our array easier:
```c
typedef struct {
    void *buffer;
    size_t capacity;
    size_t length;
    size_t item_size;
} DynamicArray;

```

- `buffer` is where our array will be stored (void * in C means that we are pointing to a block of memory with an unknown type, we could see that as "generic" since C doesn't have true generics).
- `capacity` is the actual size of our array, it denotes how many items can be stored, its the concrete array size.
- `length` is the amount of items actually stored inside the array, we can have an array with size 5, but only have 3 items inside it, meaning that we have 2 empty spaces that can be used.
- `item_size` is the size of the type T that is stored inside the array.

Because our array is "generic", we need to store the type size we are dealing with, and for the capacity and length are needed because we are using a dynamic array, we don't necessarily know upfront how many elements someone is going to store inside it, so we can optionally allocate more memory. But how about the rule for growing shrinking?

First we need to understand that when we allocate an array, the operating system needs to find a contiguous block of memory with the capacity we need to store all the elements we want, if we run out of space inside our array, and we need more space, we need to ask for the operating system yet another block of memory. Imagine that we have an array with capacity for 5 items, once a programmer request to inserted another item inside the array, the rule we've chosen to grow the array is to take the current size and multiply it by 2, so now the operating system needs to find a block of memory of size `10 * type size`, if the memory directly ahead of our array is already taken or doesn't have the necessary space, the operating system needs to find another suitable location somewhere in memory. This operation usually doesn't take too long, but what about the data already written on our previous array? We'll need to copy all the previous data to the new array, and depending on that size this can heavily penalize the programs performance.

```text
|----+----+----|
|0001|0002|0003|
|----+----+----|
  |    |    |
|----+----+----+----+----+----|
|    |    |    |    |    |    |
|----+----+----+----+----+----|

Copying the contents of the old array to the new array.
```

Thats why different projects try different grow strategies so that re-allocation of the arrays occurs less frequently, for example, the [stb_ds](https://github.com/nothings/stb/blob/master/stb_ds.h) library doubles the array size when it grows to minimize allocations, making each dynamic array created on the program to grow an average of one time per program lifespan.

Theres many ways of implementing a dynamic array, and you should consult in your language standard library or a third-party library how the dynamic array works internally so you can understand the best way to use it and to what use its most efficient. Here are two examples of dynamic array implementations in C:

- [qs_dyn_array](https://github.com/alexandreparra/quick_std/blob/master/qs_dyn_array.h) This is a very bare bones and simple implementation that I made, it doesn't implement shrinking nor changing a specific value inside the array, only appending.
- [stb_ds](https://github.com/nothings/stb/blob/master/stb_ds.h) The stb headers are a very popular header only library for C/C++, the implementation is way more sophisticated and should be the best solution if you want something production ready. This file contains hashmaps too and a fair bit of custom parameters.

I won't be showing how to implemented them directly (you can take a look at the above libraries) because I'll leave this as an exercise for the people who haven't implemented their own dynamic arrays. Basically, every language has its own different constructs, in C/C++ you'll be mostly using malloc/realloc and pointer arithmetic to easily work with the arrays raw memory and other languages may have different approaches, but allocation and reallocation of memory is fundamental in every language and even though they may not present it directly to you it's rather simple to create your own solutions.

