---
title: "Data Structures 02: Linked lists"
date: 2023-10-10T10:10:00-00:00
lastmod: 2024-06-06
tags: ["data structures"]
---

The second data structure I would like to show is a very simple but very powerful one, Linked Lists 
are a simple block structure that contains a value and a pointer to the next block, they have some 
different forms, but we'll stick with the most used variations: 
- Singly linked lists.
- Doubly linked lists.
- Circular linked lists.
- Intrusive linked lists.

Because of its heavy usage of pointers between nodes, it's a good exercise to understand and 
architect different solutions that doesn't rely on contiguous blocks of memory, even though 
interfacing with a linked list can really feel like using an array in some languages, their trade 
offs are very different.

## Singly linked lists

```text
0x0001                 0x00AA
|---------------|      |-------------|
| data: 10      | ---> | data: 32    |
| next:  0x00AA |      | next:  NULL |
|---------------|      |-------------|

A representation of a singly linked lists. At the top we have
the location in memory of the node, and inside it we have a data value
and the location of the next node, if the linked list is not "linked"
to any other block, we just use NULL.
```

Differently from arrays, linked lists are composed of individual and totally self-contained nodes. 
Arrays constrains us in a way that makes us stick to the already allocated block, once we need more 
space we'll need to create a bigger block and copy all the contents, but a linked list can be 
re-arranged and extended without the need to copy the previous nodes. It has more granular control 
for each node but that means that we need to correctly manage how blocks are connected to not lose 
any references and potentially leak memory.

The general structure of a linked list is to hold a pointer to some data type and the next pointer, 
in C we can create a pretty simple structure that can sum it all up:

```c
typedef struct node {
    void *data; // This can be "generic" or not
    struct node *next;
} Node;
```
I won't go in details with `void *data` here, its just the data that each node can point too, they 
can be of any type basically. If you've read the [previous post](https://bluesden.gitlab.io/the-code-den/posts/arrays_and_dynamic_arrays/) 
about arrays, you'll know that we need to keep the data size inside our structure if we have a 
`void *`, there are many ways to do it with a linked list, one of those is to keep a initial 
structure that can hold the list and the necessary values like:

```c
typedef struct {
    Node *head;
    size_t data_size;
} LinkedListHead;
```

Knowing the structure of a linked list we can detail two special nodes: the **head**, which is the 
first node in the chain and **tail** which is the last node. These positions are very important for 
insertions and generally working with linked lists.

Because of it's easy "composability" linked lists can be seen in different data structures, even 
though [binary trees](https://en.wikipedia.org/wiki/Binary_tree) are not linked lists, the fact 
that they have one or two pointers to other data that consequently have more pointers highly 
resembles how linked list nodes work, many data structures build upon non-contiguity and for 
certain cases we can speed a lot of operations, like insertion, as we don't need to stick to 
contiguous blocks of memory we can easily swap one node for the other without reallocation and 
copying items.

## Doubly linked lists

A doubly linked list is basically the same as a singly linked list, but now we have an additional 
pointer to the previous node.

```text
0x0001                  0x00AA                    0x00FF
|----------------|      |------------------|      |------------------|
| data: 10       | ---> | data: 32         | ---> | data: 42         |
| next:  0x00AA  |      | next: 0x00FF     |      | next:  NULL      |
| previous: NULL | <--- | previous: 0x0001 | <--- | previous: 0x00AA |
|----------------|      |------------------|      |------------------|

A representation of a doubly linked list. Each node can point to the 
next and previous list, making the first node previous point to NULL.
```

The C examples is pretty much the same from the singly linked list:

```c
typedef struct node {
    void *data;     
    struct node *next;
    struct node *previous;
} Node;
```

Because each node references the one preceding it, we can easily back-track and reverse the linked 
lists by only following the previous pointers or reordering them respectively. This makes way 
easier to insert nodes as you only need one node to have all the needed information to re-structure 
your nodes pointers.

## Circular linked lists
A circular list just denotes that we never have pointers to NULL. Circular lists can be singly or 
doubly linked, where the list last node `next` points to the first node, and the first node 
`previous` points to the last node. If you iterate over the nodes you'll always end up on the head 
again, making an infinite loop. 

```text
0x0001                 0x00AA
|---------------|      |---------------|
| data: 10      |      | data: 32      |
| next:  0x00AA | ---> | next:  0x0001 |
|---------------|      |---------------|

A circular singly linked list, the last node's next pointer point to the first again
never having a pointer to NULL.
```

Because they are circular, the concept of **head** and **tail** can make our understanding of them 
easier, as we have a reference point to make insertions and avoid infinite loops.

## Intrusive linked lists

Intrusive linked lists don't hold any data inside them, instead they are embedded inside a 
structure and hold pointers to lists inside other structs, that's why they are intrusive, but I 
won't lie to you, this is not a very simple explanation to wrap your head around at first, but an 
intrusive linked list in C would look like this:

```c
typedef struct list {
  struct list *next;
} list;

// Here the item struct contains a list of items.
// Basically items is a singly linked list that points,
// to other item types.
typedef struct item {
  int val;
  list items;
} item;

// Here we create an item, the implementation is not that important.
item *i1 = create_item(16);
item *i2 = create_item(18);

// Here is the magic, the next item in our first item list points to the address
// list inside the i2 item. Having the information of where the next list is, we can
// do some magic to discover the rest of the values inside the struct.
i1->items.next = &i2->items;
```

We can visually represent this as:

```text
0x0000                             0x00AA
|---------------------------|      |-------------------------|
| 0x0000 val: 16            |      | 0x00AA val: 18          |
| 0x0008 items.next: 0x00B2 | ---> | 0x00B2 items.next: NULL |
|---------------------------|      |-------------------------|
```

As we can see, the struct item embeds inside it a linked list, in this case a singly linked list 
that has a next pointer to another list, the trick here is that we create a second item struct, and 
instead of pointing to the item struct itself, we point to the list inside the second item struct, 
that's why we take the address `&i2->items`. Then working with the offset of the struct, we can 
determine where `val` is.

Because this can be quite a lengthy explanation, I highly recommend that you read the article below, 
which already has the best examples and explanations on the subject (which I've stolen some). It'll 
explain exactly how to work with the offsets of a C struct to get the `val` by just using the 
pointer `next` on our `i1` item:
- [Instrusive linked lists](https://www.data-structures-in-practice.com/intrusive-linked-lists/), 
explains how to implement `intrusive linked lists` and shows a real example in the Linux kernel.

As stated earlier, this can be a difficult concept to wrap your head around and requires some 
knowledge of C, just take your time and try to implement it yourself. The reason we do all this 
"magic" is to achieve better speed with linked lists in C. Some other languages already implement 
linked lists with the speed boosts but without requiring the programmer to deal with the struct 
internal memory (like C++ `std::list`).

One consideration is that intrusive linked lists can be singly linked or doubly linked 
(and circular too), the "intrusive qualifier" only applies because we don't have an extra pointer 
to a separated data (like the `void *data` from the other lists above). 

Now the examples, aside from the great explanation mentioned on the article above we have:
- [Linux kernel list.h](https://github.com/torvalds/linux/blob/master/include/linux/list.h). The 
Linux kernel implementation of a circular doubly linked list (this is the same linked list 
explained in the article above). 
- [Ryan Fleury: In defense of linked lists](https://www.rfleury.com/p/in-defense-of-linked-lists). 
In this articles Ryan goes about linked list and their powers, you'll definitely hear about 
performance issues and other detractors when speaking about linked lists, but this article explains 
much aspects that are overlooked, it can be considered more advanced, but it's a well written 
article that's easy to read.
